package sheets

type memSheet struct {
	Cells [][]interface{}
}

func NewMemSheet(cells [][]interface{}) Sheet {
	return &memSheet{
		Cells: cells,
	}
}

func (g *memSheet) Rows() int {
	return len(g.Cells)
}

func (g *memSheet) Columns() int {
	max := 0
	for _, col := range g.Cells {
		l := len(col)
		if l > max {
			max = l
		}
	}
	return max
}

func (g *memSheet) Value(row int, column int) interface{} {
	if row < 0 || row >= len(g.Cells) {
		return nil
	}
	r := g.Cells[row]
	if column < 0 || column >= len(r) {
		return nil
	}
	return r[column]
}
