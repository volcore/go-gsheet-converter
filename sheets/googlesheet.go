package sheets

import (
	"golang.org/x/net/context"
	"google.golang.org/api/sheets/v4"
)

func LoadGoogleSheet(sheetId string, readRange string) (Sheet, error) {
	ctx := context.Background()
	srv, err := sheets.NewService(ctx)
	if err != nil {
		return nil, err
	}
	resp, err := srv.Spreadsheets.Values.Get(sheetId, readRange).Do()
	if err != nil {
		return nil, err
	}
	return NewMemSheet(resp.Values), nil
}


