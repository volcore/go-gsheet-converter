package golang

import (
	"bufio"
	"fmt"
	"gitlab.com/volcore/go-gsheet-converter/sheets"
	"os"
	"path/filepath"
	"strings"
)

type CodeGenInfo struct {
	OutputFile string
	DataType   string
	Keys       [][]string
	VarName    string
	Package    string
}

func CodeGenFromParser(parser *sheets.SheetParser, info *CodeGenInfo) error {
	// Create the parent dir
	parentFolder := filepath.Dir(info.OutputFile)
	if err := os.MkdirAll(parentFolder, os.ModePerm); err != nil {
		return err
	}
	// Create the target file
	f, err := os.Create(info.OutputFile)
	if err != nil {
		return err
	}
	// Create the writer
	w := bufio.NewWriter(f)
	if _, err := fmt.Fprintf(w, "package %s\n\n", info.Package); err != nil {
		return err
	}
	// is this multi-array or single array?
	multi := len(info.Keys) > 1
	// Add comment
	if _, err := fmt.Fprintf(w, "// %s contains rows of values:\n", info.VarName); err != nil {
		return err
	}
	for keyIdx, key := range info.Keys {
		if _, err := fmt.Fprintf(w, "// [%d] %s\n", keyIdx, strings.Join(key, "; ")); err != nil {
			return err
		}
	}
	if _, err := fmt.Fprintf(w, "var %s = %s{\n", info.VarName, info.DataTypeStr(multi)); err != nil {
		return err
	}
	for row := 0; row < parser.ContentRows(); row++ {
		var members []string
		for _, key := range info.Keys {
			switch info.DataType {
			case "int64":
				members = append(members, fmt.Sprintf("%d", parser.Int64At(row, key...)))
				break
			case "int32":
				members = append(members, fmt.Sprintf("%d", parser.Int32At(row, key...)))
				break
			case "uint32":
				members = append(members, fmt.Sprintf("%d", parser.UInt32At(row, key...)))
				break
			case "float32":
				members = append(members, fmt.Sprintf("%f", parser.Float32At(row, key...)))
				break
			default:
				return fmt.Errorf("unknown datatype '%s' in golang codegen", info.DataType)
			}
		}
		if len(info.Keys) == 1 {
			// Single array
			if _, err := fmt.Fprintf(w, "\t%s,\n", members[0]); err != nil {
				return err
			}
		} else {
			// Multi array
			if _, err := fmt.Fprintf(w, "\t{%s},\n", strings.Join(members, ", ")); err != nil {
				return err
			}
		}
	}
	if _, err := fmt.Fprintf(w, "}\n"); err != nil {
		return err
	}
	// Finalize
	if err := w.Flush(); err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}
	return nil
}

func (i *CodeGenInfo) DataTypeStr(multi bool) string {
	if multi {
		return "[][]" + i.DataType
	}
	return "[]" + i.DataType
}
