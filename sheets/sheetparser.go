package sheets

import (
	"fmt"
	"strconv"
	"strings"
)

type SheetParser struct {
	sheet      Sheet
	headerRows int
}

func NewSheetParser(sheet Sheet, headerRows int) (*SheetParser, error) {
	// TODO(VS): we might want to pre-process the header:
	// - trim whitespace
	// - build a map and lengths
	// - build full tree
	return &SheetParser{
		sheet:      sheet,
		headerRows: headerRows,
	}, nil
}

func (s *SheetParser) ContentRows() int {
	return s.sheet.Rows() - s.headerRows
}

func (s *SheetParser) Sheet() Sheet {
	return s.sheet
}

func (s *SheetParser) ColIdxForKeys(keys ...string) int {
	// Make sure we only read valid rows and discard extra keys
	numKeys := len(keys)
	if numKeys > s.headerRows {
		numKeys = s.headerRows
	}
	if numKeys > s.sheet.Rows() {
		numKeys = s.sheet.Rows()
	}
	// Set min and max range for columns
	minCol := 0
	maxCol := s.sheet.Columns() - 1
	for rowIdx := 0; rowIdx < numKeys; rowIdx++ {
		key := keys[rowIdx]
		// Find the new start of the target
		newMinCol := -1
		for colIdx := minCol; colIdx <= maxCol; colIdx++ {
			keyCheck, ok := s.sheet.Value(rowIdx, colIdx).(string)
			if !ok || keyCheck != key {
				continue
			}
			// Found the start
			newMinCol = colIdx
			break
		}
		// Not found, so bail
		if newMinCol < 0 {
			return -1
		}
		// Find new max
		newMaxCol := newMinCol
		for newMaxCol < maxCol {
			value, ok := s.sheet.Value(rowIdx, newMaxCol+1).(string)
			if ok && value != "" {
				// The end has been found, an non-empty cell
				break
			}
			newMaxCol++
		}
		// Copy values over
		minCol = newMinCol
		maxCol = newMaxCol
	}
	// In the end, we return minCol
	return minCol
}

func (s *SheetParser) ValueAt(row int, keys ...string) interface{} {
	colIdx := s.ColIdxForKeys(keys...)
	if colIdx < 0 {
		return nil
	}
	return s.sheet.Value(row+s.headerRows, colIdx)
}

func (s *SheetParser) StringAt(row int, keys ...string) string {
	val := s.ValueAt(row, keys...)
	strVal, ok := val.(string)
	if ok {
		return strVal
	}
	if val == nil {
		return ""
	}
	// Use fmt to convert to string
	return fmt.Sprintf("%s", val)
}

func (s *SheetParser) Int64At(row int, keys ...string) int64 {
	val := s.ValueAt(row, keys...)
	switch val.(type) {
	case int64:
		return val.(int64)
	case float64:
		return int64(val.(float64))
	case string:
		v, _ := strconv.ParseInt(val.(string), 10, 64)
		return v
	default:
		// TODO(VS): we probably need to add more types here
		return 0
	}
}

func (s *SheetParser) Int32At(row int, keys ...string) int32 {
	return int32(s.Int64At(row, keys...))
}

func (s *SheetParser) UInt32At(row int, keys ...string) uint32 {
	return uint32(s.Int64At(row, keys...))
}

func (s *SheetParser) Float64At(row int, keys ...string) float64 {
	val := s.ValueAt(row, keys...)
	switch val.(type) {
	case int64:
		return float64(val.(int64))
	case float64:
		return val.(float64)
	case string:
		s := val.(string)
		factor := 1.0
		// If this is a percentage, remove the percentage sign and remember the factor
		if strings.HasSuffix(s, "%") {
			s = s[:len(s)-1]
			factor = 0.01
		}
		v, _ := strconv.ParseFloat(s, 64)
		return v * factor
	default:
		// TODO(VS): we probably need to add more types here
		return 0
	}
}

func (s *SheetParser) Float32At(row int, keys ...string) float32 {
	return float32(s.Float64At(row, keys...))
}
