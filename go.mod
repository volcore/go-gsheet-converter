module gitlab.com/volcore/go-gsheet-converter

go 1.16

require (
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d
	google.golang.org/api v0.52.0
)
