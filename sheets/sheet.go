package sheets

type Sheet interface {
	Rows() int
	Columns() int
	Value(row int, column int) interface{}
}