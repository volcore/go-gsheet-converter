package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/volcore/go-gsheet-converter/csharp"
)

var csharpCmd = &cobra.Command{
	Use:   "csharp",
	Short: "Outputs C# files",
	RunE:  runCsharpConverter,
}

var flagCsharpOutput string
var flagCsharpType string
var flagCsharpNamespace string
var flagCsharpClassName string

func init() {
	rootCmd.AddCommand(csharpCmd)
	csharpCmd.Flags().StringVar(&flagCsharpOutput, "out", "", "Output file")
	csharpCmd.Flags().StringVar(&flagCsharpType, "type", "", "C# data type of cells")
	csharpCmd.Flags().StringVar(&flagCsharpNamespace, "namespace", "", "C# namespace")
	csharpCmd.Flags().StringVar(&flagCsharpClassName, "classname", "", "C# class name")
	csharpCmd.MarkFlagRequired("out")
	csharpCmd.MarkFlagRequired("type")
	csharpCmd.MarkFlagRequired("varname")
}

func runCsharpConverter(cmd *cobra.Command, args []string) error {
	parser := getParser()
	if parser == nil {
		log.Error("Failed to create parser")
		return fmt.Errorf("failed to create parser")
	}
	info := &csharp.CodeGenInfo{
		OutputFile: flagCsharpOutput,
		DataType:   flagCsharpType,
		Keys:       getKeys(),
		Namespace:  flagCsharpNamespace,
		ClassName:  flagCsharpClassName,
	}
	log.WithField("file", info.OutputFile).
		WithField("type", info.DataType).
		WithField("classname", info.ClassName).
		Info("Generating code...")
	err := csharp.CodeGenFromParser(parser, info)
	if err != nil {
		log.WithField("info", info).
			WithError(err).
			Error("Failed to generate code")
		return fmt.Errorf("failed to generate code")
	}
	log.Info("Done")
	return nil
}
