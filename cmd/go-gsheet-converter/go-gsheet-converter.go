package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/volcore/go-gsheet-converter/sheets"
	"os"
	"strings"
)

var rootCmd = &cobra.Command{
	Use:   "go-gsheet-converter",
	Short: "Converts Google Sheets to different formats, generates code, etc",
}

var flagSheet string
var flagRange string
var flagNumHeader int
var flagKeys string
var flagQuiet bool

func init() {
	rootCmd.PersistentFlags().StringVar(&flagSheet, "sheet", "", "Google Sheet ID")
	rootCmd.PersistentFlags().StringVar(&flagRange, "range", "A1:ZZZ9999", "Range")
	rootCmd.PersistentFlags().BoolVar(&flagQuiet, "quiet", false, "No log output unless an error happens")
	rootCmd.PersistentFlags().StringVar(&flagKeys, "keys", "",
		"Keys to output, separated by ';'. Subkeys are separated by ',', eg. 'Vehicle,Type;Vehicle,Speed;Location'")
	rootCmd.PersistentFlags().IntVar(&flagNumHeader, "numheader", 1, "Number of header rows")
	rootCmd.MarkPersistentFlagRequired("sheet")
	rootCmd.MarkPersistentFlagRequired("keys")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func getParser() *sheets.SheetParser {
	if !flagQuiet {
		log.WithField("sheet", flagSheet).
			WithField("range", flagRange).
			Info("Downloading and parsing sheet...")
	}
	sheet, err := sheets.LoadGoogleSheet(flagSheet, flagRange)
	if err != nil {
		log.WithField("sheet", flagSheet).
			WithField("range", flagRange).
			WithError(err).
			Error("Failed to load range")
		return nil
	}
	parser, err := sheets.NewSheetParser(sheet, flagNumHeader)
	if err != nil {
		log.WithField("sheet", flagSheet).
			WithField("range", flagRange).
			WithError(err).
			Error("Failed to create parser")
		return nil
	}
	if !flagQuiet {
		log.WithField("rows", parser.ContentRows()).
			Info("Downloading and parsing done")
	}
	return parser
}

func getKeys() [][]string {
	keyLists := strings.Split(flagKeys, ";")
	var result [][]string
	for _, key := range keyLists {
		result = append(result, strings.Split(key, ","))
	}
	return result
}
