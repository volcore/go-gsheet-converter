package sheets

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMemSheetEmpty(t *testing.T) {
	empty := NewMemSheet([][]interface{}{})
	assert.EqualValues(t, 0, empty.Rows())
	assert.EqualValues(t, 0, empty.Columns())
	// Test secure access
	for row := -1; row <= 2; row++ {
		for col := -1; col <= 2; col++ {
			assert.Nil(t, empty.Value(row, col))
		}
	}
}

func TestMemSheetOneRow(t *testing.T) {
	sheet := NewMemSheet([][]interface{} {
			{"test", 1, true, 2.0},
		})
	assert.EqualValues(t, 1, sheet.Rows())
	assert.EqualValues(t, 4, sheet.Columns())
	assert.EqualValues(t, "test", sheet.Value(0, 0))
	assert.IsType(t, "", sheet.Value(0, 0))
	assert.EqualValues(t, 1, sheet.Value(0, 1))
	assert.IsType(t, 2, sheet.Value(0, 1))
	assert.EqualValues(t, true, sheet.Value(0, 2))
	assert.IsType(t, false, sheet.Value(0, 2))
	assert.EqualValues(t, 2.0, sheet.Value(0, 3))
	assert.IsType(t, 1.5, sheet.Value(0, 3))
	// Test secure access
	for row := -1; row <= 2; row++ {
		for col := -1; col <= 5; col++ {
			if row == 0 && col >= 0 && col <= 3 {
				continue
			}
			assert.Nil(t, sheet.Value(row, col))
		}
	}
}
