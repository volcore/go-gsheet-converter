package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/volcore/go-gsheet-converter/golang"
	"path/filepath"
)

var golangCmd = &cobra.Command{
	Use:   "golang",
	Short: "Outputs golang files",
	RunE:  runGolangConverter,
}

var flagGolangOutput string
var flagGolangType string
var flagGolangVarName string
var flagGolangPackage string

func init() {
	rootCmd.AddCommand(golangCmd)
	golangCmd.Flags().StringVar(&flagGolangOutput, "out", "", "Output file")
	golangCmd.Flags().StringVar(&flagGolangType, "type", "", "Go data type of cells")
	golangCmd.Flags().StringVar(&flagGolangVarName, "varname", "", "Go variable name")
	golangCmd.Flags().StringVar(&flagGolangVarName, "package", "", "Go package")
	golangCmd.MarkFlagRequired("out")
	golangCmd.MarkFlagRequired("type")
	golangCmd.MarkFlagRequired("varname")
}

func runGolangConverter(cmd *cobra.Command, args []string) error {
	parser := getParser()
	if parser == nil {
		log.Error("Failed to create parser")
		return fmt.Errorf("failed to create parser")
	}
	info := &golang.CodeGenInfo{
		OutputFile: flagGolangOutput,
		DataType:   flagGolangType,
		Keys:       getKeys(),
		VarName:    flagGolangVarName,
		Package:    goPackage(),
	}
	log.WithField("file", info.OutputFile).
		WithField("type", info.DataType).
		WithField("varname", info.VarName).
		Info("Generating code...")
	err := golang.CodeGenFromParser(parser, info)
	if err != nil {
		log.WithField("info", info).
			WithError(err).
			Error("Failed to generate code")
		return fmt.Errorf("failed to generate code")
	}
	log.Info("Done")
	return nil
}

func goPackage() string {
	if flagGolangPackage != "" {
		return flagGolangPackage
	}
	dir := filepath.Dir(flagGolangOutput)
	return filepath.Base(dir)
}
