package csharp

import (
	"bufio"
	"fmt"
	"gitlab.com/volcore/go-gsheet-converter/sheets"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type CodeGenInfo struct {
	OutputFile string
	DataType   string
	Keys       [][]string
	Namespace  string
	ClassName  string
}

func CodeGenFromParser(parser *sheets.SheetParser, info *CodeGenInfo) error {
	// Create the parent dir
	parentFolder := filepath.Dir(info.OutputFile)
	if err := os.MkdirAll(parentFolder, os.ModePerm); err != nil {
		return err
	}
	// Create the target file
	f, err := os.Create(info.OutputFile)
	if err != nil {
		return err
	}
	// Create the writer
	w := bufio.NewWriter(f)
	if _, err := fmt.Fprintf(w, "namespace %s {\n", info.Namespace); err != nil {
		return err
	}
	if info.DataType == "enum" {
		if len(info.Keys) != 1 {
			return fmt.Errorf("datatype enum only works with a single key")
		}
		if _, err := fmt.Fprintf(w, "  public enum %s {\n", info.ClassName); err != nil {
			return err
		}
		for keyIdx, key := range info.Keys {
			if _, err := fmt.Fprintf(w, "    // [%d] %s\n", keyIdx, strings.Join(key, "; ")); err != nil {
				return err
			}
		}
		for row := 0; row < parser.ContentRows(); row++ {
			val := parser.StringAt(row, info.Keys[0]...)
			if _, err := fmt.Fprintf(w, "      %s = %d,\n", val, row); err != nil {
				return err
			}
		}
		if _, err := fmt.Fprintf(w, "  }\n"); err != nil {
			return err
		}
	} else {
		if _, err := fmt.Fprintf(w, "  public static class %s {\n", info.ClassName); err != nil {
			return err
		}
		for keyIdx, key := range info.Keys {
			if _, err := fmt.Fprintf(w, "    // [%d] %s\n", keyIdx, strings.Join(key, "; ")); err != nil {
				return err
			}
		}
		if _, err := fmt.Fprintf(w, "    public static readonly %s[] Values = new[] {\n", info.DataType); err != nil {
			return err
		}
		for row := 0; row < parser.ContentRows(); row++ {
			var members []string
			for _, key := range info.Keys {
				switch info.DataType {
				case "int":
					members = append(members, fmt.Sprintf("%d", parser.Int32At(row, key...)))
					break
				case "string":
					members = append(members, escapeString(parser.StringAt(row, key...)))
				default:
					return fmt.Errorf("unknown datatype '%s' in csharp codegen", info.DataType)
				}
			}
			if _, err := fmt.Fprintf(w, "      %s,\n", strings.Join(members, ", ")); err != nil {
				return err
			}
		}
		if _, err := fmt.Fprintf(w, "    };\n"); err != nil {
			return err
		}
		if _, err := fmt.Fprintf(w, "  }\n"); err != nil {
			return err
		}
	}
	if _, err := fmt.Fprintf(w, "}\n"); err != nil {
		return err
	}
	// Finalize
	if err := w.Flush(); err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}
	return nil
}

func escapeString(val string) string {
	return strconv.QuoteToASCII(val)
}
