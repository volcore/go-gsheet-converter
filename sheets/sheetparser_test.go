package sheets

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSheetParserValueAt(t *testing.T) {
	sheet := NewMemSheet([][]interface{}{
		{"First", "", "", "Second", "Third col", "", "4th", "5th"},
		{"sub1", "", "sub3", "", "a", "b", "", "c"},
		{"First:sub1", "First:", "First:sub3", "Second", "Third col:a", "Third col:b", "4th", "5th:c"},
		{1, 2, 3, 4, 5, 6, 7, 8},
		{1, 2, 4, 8},
	})
	p := NewSheetParser(sheet, 2)
	// First
	assert.EqualValues(t, "First:sub1", p.ValueAt(0, "First"))
	assert.EqualValues(t, "First:sub1", p.ValueAt(0, "First", "sub1"))
	assert.EqualValues(t, "First:sub1", p.ValueAt(0, "First", "sub1", "asasdfasdfnything"))
	assert.EqualValues(t, "First:", p.ValueAt(0, "First", ""))
	assert.EqualValues(t, "First:sub3", p.ValueAt(0, "First", "sub3"))
	assert.EqualValues(t, nil, p.ValueAt(0, "First", "anythingelse"))
	assert.EqualValues(t, 1, p.ValueAt(1, "First"))
	assert.EqualValues(t, 1, p.ValueAt(2, "First"))
	assert.EqualValues(t, nil, p.ValueAt(3, "First"))
	assert.EqualValues(t, 3, p.ValueAt(1, "First", "sub3"))
	assert.EqualValues(t, 4, p.ValueAt(2, "First", "sub3"))
	assert.EqualValues(t, nil, p.ValueAt(3, "First", "sub3"))
	// Second
	assert.EqualValues(t, "Second", p.ValueAt(0, "Second"))
	assert.EqualValues(t, "Second", p.ValueAt(0, "Second", ""))
	assert.EqualValues(t, 4, p.ValueAt(1, "Second"))
	assert.EqualValues(t, 8, p.ValueAt(2, "Second"))
	assert.EqualValues(t, nil, p.ValueAt(3, "Second"))
	assert.EqualValues(t, nil, p.ValueAt(0, "Second", "wrong"))
	// Third
	assert.EqualValues(t, "Third col:a", p.ValueAt(0, "Third col"))
	assert.EqualValues(t, "Third col:a", p.ValueAt(0, "Third col", "a"))
	assert.EqualValues(t, "Third col:b", p.ValueAt(0, "Third col", "b"))
	assert.EqualValues(t, 5, p.ValueAt(1, "Third col"))
	assert.EqualValues(t, nil, p.ValueAt(2, "Third col"))
	assert.EqualValues(t, nil, p.ValueAt(3, "Third col"))
	assert.EqualValues(t, 5, p.ValueAt(1, "Third col", "a"))
	assert.EqualValues(t, nil, p.ValueAt(2, "Third col", "a"))
	assert.EqualValues(t, nil, p.ValueAt(3, "Third col", "a"))
	assert.EqualValues(t, 6, p.ValueAt(1, "Third col", "b"))
	assert.EqualValues(t, nil, p.ValueAt(2, "Third col", "b"))
	assert.EqualValues(t, nil, p.ValueAt(3, "Third col", "b"))
	assert.EqualValues(t, nil, p.ValueAt(0, "Third col", "A"))
	assert.EqualValues(t, nil, p.ValueAt(0, "Third col", "c"))
	// Fourth
	assert.EqualValues(t, "4th", p.ValueAt(0, "4th"))
	assert.EqualValues(t, 7, p.ValueAt(1, "4th"))
	assert.EqualValues(t, nil, p.ValueAt(2, "4th"))
	assert.EqualValues(t, "4th", p.ValueAt(0, "4th", ""))
	assert.EqualValues(t, nil, p.ValueAt(0, "4th", "asdfasdf"))
	// Fifth
	assert.EqualValues(t, "5th:c", p.ValueAt(0, "5th"))
	assert.EqualValues(t, 8, p.ValueAt(1, "5th"))
	assert.EqualValues(t, nil, p.ValueAt(2, "5th"))
	assert.EqualValues(t, "5th:c", p.ValueAt(0, "5th", "c"))
	assert.EqualValues(t, nil, p.ValueAt(0, "5th", ""))
	// Special case
	assert.EqualValues(t, "First:", p.ValueAt(0, ""))
	assert.EqualValues(t, "First:", p.ValueAt(0, "", ""))
	assert.EqualValues(t, "First:sub3", p.ValueAt(0, "", "sub3"))
	assert.EqualValues(t, 2, p.ValueAt(1, ""))
	assert.EqualValues(t, 2, p.ValueAt(1, "", ""))
	assert.EqualValues(t, 3, p.ValueAt(1, "", "sub3"))
	assert.EqualValues(t, 2, p.ValueAt(2, ""))
	assert.EqualValues(t, 2, p.ValueAt(2, "", ""))
	assert.EqualValues(t, 4, p.ValueAt(2, "", "sub3"))
	assert.EqualValues(t, nil, p.ValueAt(3, ""))
	assert.EqualValues(t, nil, p.ValueAt(3, "", ""))
	assert.EqualValues(t, nil, p.ValueAt(3, "", "sub3"))
}

func TestSheetParserColIdx(t *testing.T) {
	sheet := NewMemSheet([][]interface{}{
		{"First", "", "", "Second", "Third col", "", "4th", "5th"},
		{"sub1", "", "sub3", "", "a", "b", "", "c"},
	})
	p := NewSheetParser(sheet, 2)
	// Length 0. Always returns the first column, as that's the normal pattern (when n+1 key is not specified)
	assert.EqualValues(t, 0, p.ColIdxForKeys())
	// Length 1
	assert.EqualValues(t, 0, p.ColIdxForKeys("First"))
	assert.EqualValues(t, 1, p.ColIdxForKeys(""))
	assert.EqualValues(t, 3, p.ColIdxForKeys("Second"))
	assert.EqualValues(t, 4, p.ColIdxForKeys("Third col"))
	assert.EqualValues(t, 6, p.ColIdxForKeys("4th"))
	assert.EqualValues(t, 7, p.ColIdxForKeys("5th"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("asdfasd"))
	// Length 2
	assert.EqualValues(t, 0, p.ColIdxForKeys("First", "sub1"))
	assert.EqualValues(t, 1, p.ColIdxForKeys("First", ""))
	assert.EqualValues(t, 2, p.ColIdxForKeys("First", "sub3"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("First", "asdfasdfkl"))
	assert.EqualValues(t, 1, p.ColIdxForKeys("", ""))
	assert.EqualValues(t, 2, p.ColIdxForKeys("", "sub3"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("", "sub1"))
	assert.EqualValues(t, 3, p.ColIdxForKeys("Second", ""))
	assert.EqualValues(t, -1, p.ColIdxForKeys("Second", "a"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("Second", "sub3"))
	assert.EqualValues(t, 4, p.ColIdxForKeys("Third col", "a"))
	assert.EqualValues(t, 5, p.ColIdxForKeys("Third col", "b"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("Third col", "sub3"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("Third col", ""))
	assert.EqualValues(t, 6, p.ColIdxForKeys("4th", ""))
	assert.EqualValues(t, -1, p.ColIdxForKeys("4th", "b"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("4th", "asdf"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("4th", "c"))
	assert.EqualValues(t, 7, p.ColIdxForKeys("5th", "c"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("5th", "asdf"))
	assert.EqualValues(t, -1, p.ColIdxForKeys("5th", ""))
	// Length 3 (these are ignored, as the header isn't long enough
	assert.EqualValues(t, 0, p.ColIdxForKeys("First", "sub1", "asdf"))
}
